/*
 * *
 *  * Created by Mohammed Akram Hussain on 03/10/20 13:03
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 03/10/20 12:16
 *
 */

package com.prophettech.europegate.rest;

public class Services {

    public static final String CONSUMER_SECRET = "cs_5cdc12b838e11d57868d25c83dd9e7509a852e54";
    public static final String CONSUMER_KEY = "ck_c2e278c2f11a9cde6ab64d0c2897119b0bece774";

    public static final String BASE_URL = "https://europegate.co.uk/";
    public static final String GET_ALL_PRODUCTS = "wp-json/wc/v3/products";
    public static final String GET_ALL_CATEGORIES = "wp-json/wc/v3/products/categories";
    public static final String GET_PRODUCTS_BY_CATEGORY = "wp-json/wc/v3/products?per_page=100&category=";
    public static final String GET_PRODUCT_BY_ID = "wp-json/wc/v3/products/";
    public static final String PLACE_ORDER = "wp-json/wc/v3/orders";
    public static final String ORDER_STATUS = "fcm/orders.json";
}
