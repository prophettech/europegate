/*
 * *
 *  * Created by Mohammed Akram Hussain on 03/10/20 13:03
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 03/10/20 13:03
 *
 */

package com.prophettech.europegate.rest.data;

import android.util.Log;

import com.prophettech.europegate.models.CategoriesModel;
import com.prophettech.europegate.models.ProductsModel;
import com.prophettech.europegate.rest.NetworkCaller;
import com.prophettech.europegate.rest.Services;
import com.prophettech.europegate.rest.URLBuilder;
import com.prophettech.europegate.rest.WebConstant;
import com.prophettech.europegate.utils.AppConstants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

public class RemoteDataSource extends DataSource {

    private static final String TAG = RemoteDataSource.class.getSimpleName();
    private static RemoteDataSource instance = null;

    private RemoteDataSource() {

    }

    public static RemoteDataSource getInstance() {
        if (instance == null) {
            instance = new RemoteDataSource();
        }
        return instance;
    }

    @Override
    public void getAllCategories(GetCategoriesCallback callback) {
        NetworkCaller.makeRequestForJsonArray(WebConstant.GET, URLBuilder.getCategoriesURL(Services.GET_ALL_CATEGORIES),
                null, response -> {
                    responseLogger(response);
                    if (response != null) {
                        List<CategoriesModel> categoriesList = ResponseHandler.getAllCategories(response);
                        if (categoriesList != null) {
                            callback.onSuccess(categoriesList);
                        } else {
                            callback.onFailure(new NullPointerException(AppConstants.errorMessage));
                        }
                    } else {
                        callback.onFailure(new Exception(AppConstants.errorMessage));
                    }
                }, callback::onFailure, TAG);
    }

    @Override
    public void getProductsByCategory(double id, GetProductsByCategoryCallback callback) {
        NetworkCaller.makeRequestForJsonArray(WebConstant.GET, URLBuilder.getProdcutsWithIdURL(Services.GET_PRODUCTS_BY_CATEGORY, id),
                null, response -> {
                    responseLogger(response);
                    if (response != null) {
                        List<ProductsModel> categoriesList = ResponseHandler.getProducts(response);
                        if (categoriesList != null) {
                            callback.onSuccess(categoriesList);
                        } else {
                            callback.onFailure(new NullPointerException(AppConstants.errorMessage));
                        }
                    } else {
                        callback.onFailure(new Exception(AppConstants.errorMessage));
                    }
                }, callback::onFailure, TAG);
    }

    @Override
    public void checkOrderStatus(CommonCallback<Integer> callback) {
        NetworkCaller.makeRequestForJsonObject(WebConstant.GET, URLBuilder.getOrderStatusURL(Services.ORDER_STATUS),
                false, null, TAG,
                response -> {
                    responseLogger(response);
                    if (response != null) {
                        int data = response.optInt("ordersOpen");
                        callback.onSuccess(data);
                    }
                }, callback::onFailure);
    }

    private void responseLogger(JSONObject response) {
        if (response != null) {
            Log.d(TAG, response.toString());
        }
    }

    private void responseLogger(JSONArray response) {
        if (response != null) {
            Log.d(TAG, response.toString());
        }
    }
}
