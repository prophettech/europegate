/*
 * *
 *  * Created by Mohammed Akram Hussain on 03/10/20 13:03
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 03/10/20 13:03
 *
 */

package com.prophettech.europegate.rest.data;

import com.prophettech.europegate.models.CategoriesModel;
import com.prophettech.europegate.models.ProductsModel;

import java.util.List;

public abstract class DataSource {

    public abstract void getAllCategories(GetCategoriesCallback callback);

    public abstract void getProductsByCategory(double id, GetProductsByCategoryCallback callback);

    public abstract void checkOrderStatus(CommonCallback<Integer> callback);

    public interface CommonCallback<T> extends IFailure {
        void onSuccess(T t);
    }

    public interface GetCategoriesCallback extends IFailure {
        void onSuccess(List<CategoriesModel> categories);
    }

    public interface GetProductsByCategoryCallback extends IFailure {
        void onSuccess(List<ProductsModel> products);
    }

    private interface IFailure {
        void onFailure(Throwable throwable);

        void onNetworkFailure();
    }
}
