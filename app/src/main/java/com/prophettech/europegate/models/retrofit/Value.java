
/*
 * *
 *  * Created by Mohammed Akram Hussain on 03/10/20 13:03
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 03/10/20 12:16
 *
 */

package com.prophettech.europegate.models.retrofit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Value {

    @SerializedName("tmfbuilder")
    @Expose
    private Tmfbuilder tmfbuilder;

    public Tmfbuilder getTmfbuilder() {
        return tmfbuilder;
    }

    public void setTmfbuilder(Tmfbuilder tmfbuilder) {
        this.tmfbuilder = tmfbuilder;
    }

}
