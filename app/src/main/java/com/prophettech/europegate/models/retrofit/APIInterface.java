/*
 * *
 *  * Created by Mohammed Akram Hussain on 03/10/20 13:03
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 03/10/20 13:03
 *
 */

package com.prophettech.europegate.models.retrofit;

import com.prophettech.europegate.models.billing.Order;
import com.prophettech.europegate.models.billing.OrderPlacedResponse;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface APIInterface {

    @GET("wp-json/wc/v3/products/{id}")
    Call<Model> getProductDetails(@Path("id") int productId, @Query("consumer_key") String consumerKey, @Query("consumer_secret") String consumerSecret);

    @POST("wp-json/wc/v3/orders")
    Call<OrderPlacedResponse> placeOrder(@Body Order order, @Query("consumer_key") String consumerKey, @Query("consumer_secret") String consumerSecret);

    @POST("wp-json/wc/v3/orders/{id}/notes")
    Call<NotesResponse> addOrderNotes(@Path("id") int orderId, @Body NotesBody body, @Query("consumer_key") String consumerKey, @Query("consumer_secret") String consumerSecret);

    @FormUrlEncoded
    @POST("fcm/inserttoken.php")
    Call<ResponseBody> insertToken(@Field("id") String id, @Field("fcmtoken") String mytoken);
}
