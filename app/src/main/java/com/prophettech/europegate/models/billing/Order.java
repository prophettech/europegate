/*
 * *
 *  * Created by Mohammed Akram Hussain on 03/10/20 13:03
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 03/10/20 12:16
 *
 */

package com.prophettech.europegate.models.billing;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Order {

    @SerializedName("payment_method")
    private String paymentMethod;
    @SerializedName("payment_method_title")
    private String paymentMethodTitle;
    @SerializedName("set_paid")
    private boolean setPaid;
    @SerializedName("billing")
    private BillingModel billing;
    @SerializedName("shipping")
    private BillingModel shipping;
    @SerializedName("line_items")
    private List<LineItems> lineItems;
    @SerializedName("shipping_lines")
    private List<ShippingLines> shippingLines;
    @SerializedName("coupon_lines")
    private List<CouponLines> couponLines;

    public List<CouponLines> getCouponLines() {
        return couponLines;
    }

    public void setCouponLines(List<CouponLines> couponLines) {
        this.couponLines = couponLines;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getPaymentMethodTitle() {
        return paymentMethodTitle;
    }

    public void setPaymentMethodTitle(String paymentMethodTitle) {
        this.paymentMethodTitle = paymentMethodTitle;
    }

    public boolean isSetPaid() {
        return setPaid;
    }

    public void setSetPaid(boolean setPaid) {
        this.setPaid = setPaid;
    }

    public BillingModel getBilling() {
        return billing;
    }

    public void setBilling(BillingModel billing) {
        this.billing = billing;
    }

    public BillingModel getShipping() {
        return shipping;
    }

    public void setShipping(BillingModel shipping) {
        this.shipping = shipping;
    }

    public List<LineItems> getLineItems() {
        return lineItems;
    }

    public void setLineItems(List<LineItems> lineItems) {
        this.lineItems = lineItems;
    }

    public List<ShippingLines> getShippingLines() {
        return shippingLines;
    }

    public void setShippingLines(List<ShippingLines> shippingLines) {
        this.shippingLines = shippingLines;
    }
}
