/*
 * *
 *  * Created by Mohammed Akram Hussain on 03/10/20 13:03
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 03/10/20 12:16
 *
 */

package com.prophettech.europegate.models;

import java.util.List;

public class OptionsModel {

    private List<OptionsItemModel> items;

    public List<OptionsItemModel> getItems() {
        return items;
    }

    public void setItems(List<OptionsItemModel> items) {
        this.items = items;
    }
}
