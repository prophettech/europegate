/*
 * *
 *  * Created by Mohammed Akram Hussain on 03/10/20 13:03
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 03/10/20 12:16
 *
 */

package com.prophettech.europegate.threading;

import android.os.Handler;
import android.os.Looper;

public class MainThread {
    private static MainThread instance = null;
    private Handler handler;

    private MainThread() {
        handler = new Handler(Looper.getMainLooper());
    }

    public static MainThread getInstance() {
        if (instance == null) {
            instance = new MainThread();
        }
        return instance;
    }

    public void post(Runnable runnable) {
        handler.post(runnable);
    }

    public void postDelayed(Runnable runnable, long i) {
        handler.postDelayed(runnable, i);
    }
}
