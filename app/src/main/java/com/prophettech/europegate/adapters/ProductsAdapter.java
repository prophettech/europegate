/*
 * *
 *  * Created by Mohammed Akram Hussain on 03/10/20 13:03
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 03/10/20 13:03
 *
 */

package com.prophettech.europegate.adapters;

import android.content.Context;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.palette.graphics.Palette;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.github.florent37.glidepalette.GlidePalette;
import com.prophettech.europegate.R;
import com.prophettech.europegate.models.ProductsModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ProductsViewHolder> {

    private List<ProductsModel> mProductsList;
    private ProductClickListener mProductClickListener;
    private Context mContext;

    public ProductsAdapter() {
        mProductsList = new ArrayList<>();
    }

    public void refresh(List<ProductsModel> productsList, ProductClickListener clickListener) {
        mProductsList = productsList;
        mProductClickListener = clickListener;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ProductsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.categories_list_item, parent, false);
        mContext = parent.getContext();
        return new ProductsAdapter.ProductsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductsViewHolder holder, int position) {
        ProductsModel item = mProductsList.get(position);
        holder.mProductName.setText(item.getName());
        holder.mProductDescription.setText(Html.fromHtml(item.getDescription()).toString());
        if (!TextUtils.isEmpty(item.getImage().getSrc())) {
            Glide.with(mContext)
                    .load(item.getImage().getSrc())
                    .listener(GlidePalette.with(item.getImage().getSrc())
                            .intoCallBack(palette -> {
                                Palette.Swatch swatch = palette.getVibrantSwatch();
                                if (swatch != null) {
                                    holder.mProductCardView.setBackgroundColor(swatch.getRgb());
                                    holder.mProductName.setTextColor(swatch.getBodyTextColor());
                                    holder.mProductDescription.setTextColor(swatch.getBodyTextColor());
                                }
                            })).into(holder.mProductImage);
        }
        holder.mProductCardView.setOnClickListener(v -> mProductClickListener.onProductClick(item.getId()));
    }

    @Override
    public int getItemCount() {
        return mProductsList.size();
    }

    public interface ProductClickListener {
        void onProductClick(double id);
    }

    static class ProductsViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.categoryImageView)
        ImageView mProductImage;
        @BindView(R.id.categoryName)
        TextView mProductName;
        @BindView(R.id.categoryDescription)
        TextView mProductDescription;
        @BindView(R.id.categoryCardView)
        CardView mProductCardView;


        ProductsViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
