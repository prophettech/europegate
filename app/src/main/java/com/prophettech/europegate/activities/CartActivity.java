/*
 * *
 *  * Created by Mohammed Akram Hussain on 03/10/20 13:03
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 03/10/20 13:03
 *
 */

package com.prophettech.europegate.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.prophettech.europegate.R;
import com.prophettech.europegate.adapters.CartAdapter;
import com.prophettech.europegate.base.BaseActivity;
import com.prophettech.europegate.models.cart.Cart;
import com.prophettech.europegate.models.cart.CartItem;
import com.prophettech.europegate.utils.CommonUtils;
import com.prophettech.europegate.utils.PaperUtil;

import butterknife.BindView;

public class CartActivity extends BaseActivity implements CartAdapter.CartInterface {

    @BindView(R.id.cartRecyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.tvTotalPrice)
    TextView mTotalPrice;
    @BindView(R.id.tvTotalQuantity)
    TextView mTotalQuantity;
    @BindView(R.id.tvNoItems)
    TextView mNoItems;

    private CartAdapter mAdapter;
    private int quantity = 0;
    private double price = 0;

    @SuppressLint("MissingSuperCall")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        onCreate(savedInstanceState, R.layout.activity_cart);

        mRecyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
        mAdapter = new CartAdapter(this, false);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this, LinearLayoutManager.VERTICAL);
        mRecyclerView.addItemDecoration(dividerItemDecoration);
        mRecyclerView.setAdapter(mAdapter);
        setData();
    }

    private void setData() {
        Cart items = PaperUtil.getCartItems();
        if (items != null && items.getProducts().size() > 0) {
            quantity = 0;
            price = 0;
            mAdapter.refresh(items.getProducts());
            mNoItems.setVisibility(View.GONE);
            for (CartItem item : items.getProducts()) {
                quantity = quantity + item.getQuantity();
                price = price + item.getPrice();
            }
            items.setTotalQuantity(quantity);
            items.setTotalPrice(price);
            PaperUtil.updateCart(items);
            mTotalQuantity.setText(String.valueOf(items.getTotalQuantity()));
            mTotalPrice.setText("￡" + CommonUtils.getTwoDecimal(items.getTotalPrice()));
            if (items.getTotalQuantity() <= 0) {
                reset();
                return;
            }
        }
    }

    @Override
    public void onToolBarSetUp(Toolbar toolbar, ActionBar actionBar) {
        actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);
        TextView textView = toolbar.findViewById(R.id.tvHeading);
        textView.setText(R.string.cart_title);
        ImageButton cart = toolbar.findViewById(R.id.cartView);
        cart.setVisibility(View.GONE);
    }

    @Override
    public void onAddClick(CartItem cartItem, int position) {
        int quantity = cartItem.getQuantity();
        double price = cartItem.getOriginalPrice();
        if (quantity < 99) {
            quantity += 1;
            cartItem.setQuantity(quantity);
            cartItem.setPrice(price * quantity);
            PaperUtil.updateCartItem(cartItem);
        }
        mAdapter.refresh(PaperUtil.getCartItems().getProducts());
        setData();
    }

    @Override
    public void onRemoveClick(CartItem cartItem, int position) {
        int quantity = cartItem.getQuantity();
        double price = cartItem.getOriginalPrice();
        if (quantity > 0) {
            quantity -= 1;
            if (quantity == 0) {
                PaperUtil.deleteItem(cartItem);
                setData();
                mAdapter.refresh(PaperUtil.getCartItems().getProducts());
                return;
            }
            cartItem.setQuantity(quantity);
            cartItem.setPrice(price * quantity);
            PaperUtil.updateCartItem(cartItem);
        }
        mAdapter.refresh(PaperUtil.getCartItems().getProducts());
        setData();
    }

    public void checkOut(View view) {
        if (quantity > 0) {
            Intent intent = new Intent(this, CheckOutActivity.class);
            startActivity(intent);
        } else {
            showToastMessage("Please Add Items to Cart!");
        }
    }

    private void reset() {
        mNoItems.setVisibility(View.VISIBLE);
        mTotalPrice.setText("---");
        mTotalQuantity.setText("--");
        this.quantity = 0;
        this.price = 0;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
