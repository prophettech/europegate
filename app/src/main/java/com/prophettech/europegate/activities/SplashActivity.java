/*
 * *
 *  * Created by Mohammed Akram Hussain on 03/10/20 13:03
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 03/10/20 13:03
 *
 */

package com.prophettech.europegate.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;

import com.google.firebase.messaging.FirebaseMessaging;
import com.prophettech.europegate.R;
import com.prophettech.europegate.base.BaseActivity;
import com.prophettech.europegate.models.retrofit.RetrofitClientInstance;
import com.prophettech.europegate.rest.data.DataSource;
import com.prophettech.europegate.rest.data.RemoteDataSource;
import com.prophettech.europegate.utils.CommonUtils;
import com.prophettech.europegate.utils.OverlayDialog;
import com.prophettech.europegate.utils.PaperUtil;
import com.prophettech.europegate.utils.services.FirebaseService;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

import butterknife.BindView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends BaseActivity {

    @BindView(R.id.carousalView)
    CarouselView mCarouselView;
    @BindView(R.id.link)
    TextView mLink;
    int[] sampleImages = {R.drawable.carousel1, R.drawable.carousel2, R.drawable.carousel3, R.drawable.carousel4};
    private OverlayDialog overlayDialog;
    private boolean mOrderStatus = false;

    @SuppressLint("MissingSuperCall")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        onCreate(savedInstanceState, R.layout.activity_splash);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }
        initViews();
        initFirebase();
        checkForOrdersStatus();
    }

    private void checkForOrdersStatus() {
        RemoteDataSource.getInstance().checkOrderStatus(new DataSource.CommonCallback<Integer>() {
            @Override
            public void onSuccess(Integer integer) {
                if (integer == 1) {
                    mOrderStatus = true;
                }
                if (integer == 0) {
                    mOrderStatus = false;
                }
            }

            @Override
            public void onFailure(Throwable throwable) {

            }

            @Override
            public void onNetworkFailure() {

            }
        });
    }

    private void initFirebase() {
        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        Log.w(TAG, "getInstanceId failed", task.getException());
                        return;
                    }

                    // Get new Instance ID token
                    String token = task.getResult();
                    if (!TextUtils.isEmpty(token) && !PaperUtil.isFirebaseTokenUpdated()) {
                        FirebaseService.sendRegistrationToServer(token);
                    }
                    // Log and toast
                    String msg = "";
                    //getString(R.string.msg_token_fmt, token);
                    Log.d(TAG, msg);
                    Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                });
    }

    private void initViews() {
        mLink.setMovementMethod(LinkMovementMethod.getInstance());
        mCarouselView.setPageCount(sampleImages.length);
        ImageListener imageListener = (position, imageView) -> imageView.setImageResource(sampleImages[position]);
        mCarouselView.setImageListener(imageListener);
    }

    public void orderOnline(View view) {
        if (!CommonUtils.isNetworkAvailable(this)) {
            showNetworkDialog();
        } else {
            if (mOrderStatus) {
                showHomeActivity();
            } else {
                showOrdersClosedActivity();
            }
        }
    }

    private void showOrdersClosedActivity() {
        Intent intent = new Intent(this, ClosedOrders.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!CommonUtils.isNetworkAvailable(this)) {
            showNetworkDialog();
        }
    }

    private void showHomeActivity() {
        Intent intent = new Intent(this, CategoriesActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    private void showNetworkDialog() {
        if (overlayDialog != null && overlayDialog.isVisible()) {
            overlayDialog.dismissDialog();
        }
        overlayDialog = new OverlayDialog(this);
        overlayDialog.setHeading("No Network");
        overlayDialog.setButtonText("Okay");
        overlayDialog.setMessage("Make sure you are connected to a proper internet connection and then try again.");
        overlayDialog.showDialog();
    }

    @Override
    public void onToolBarSetUp(Toolbar toolbar, ActionBar actionBar) {
        TextView textView = toolbar.findViewById(R.id.tvHeading);
        textView.setText(R.string.home);
        ImageButton cart = toolbar.findViewById(R.id.cartView);
        cart.setVisibility(View.GONE);
    }

    public void sendRegistrationToServer(String token) {
        TelephonyManager telephonyManager;
        telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        String uniqueID = null;
        if (telephonyManager != null) {
            uniqueID = telephonyManager.getDeviceId();
        }
        Call<ResponseBody> call = RetrofitClientInstance.getInstance().getMyApi().insertToken(uniqueID, token);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                PaperUtil.setFirebaseTokenUpdated(true);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }
}