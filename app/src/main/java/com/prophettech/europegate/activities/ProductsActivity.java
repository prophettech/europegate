/*
 * *
 *  * Created by Mohammed Akram Hussain on 03/10/20 13:03
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 03/10/20 13:03
 *
 */

package com.prophettech.europegate.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.prophettech.europegate.R;
import com.prophettech.europegate.adapters.ProductsAdapter;
import com.prophettech.europegate.base.BaseActivity;
import com.prophettech.europegate.models.ProductsModel;
import com.prophettech.europegate.rest.data.DataSource;
import com.prophettech.europegate.rest.data.RemoteDataSource;

import java.util.Comparator;
import java.util.List;

import butterknife.BindView;

public class ProductsActivity extends BaseActivity implements ProductsAdapter.ProductClickListener {

    @BindView(R.id.productsRecyclerView)
    RecyclerView mRecyclerView;

    private ProductsAdapter mAdapter;
    private double mCategoryId;

    @SuppressLint("MissingSuperCall")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        onCreate(savedInstanceState, R.layout.activity_products);

        mCategoryId = getIntent().getDoubleExtra("CATEGORY_ID", 0);

        mRecyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
        mAdapter = new ProductsAdapter();
        mRecyclerView.setAdapter(mAdapter);

        getProducts();
    }

    private void getProducts() {
        showProgress(true);
        RemoteDataSource.getInstance().getProductsByCategory(mCategoryId, new DataSource.GetProductsByCategoryCallback() {
            @Override
            public void onSuccess(List<ProductsModel> products) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    products.sort(Comparator.comparing(ProductsModel::getMenuOrder));
                }
                mAdapter.refresh(products, ProductsActivity.this);
                showProgress(false);
            }

            @Override
            public void onFailure(Throwable throwable) {

            }

            @Override
            public void onNetworkFailure() {

            }
        });
    }


    @Override
    public void onProductClick(double id) {
        Intent intent = new Intent(getBaseContext(), DetailActivity.class);
        intent.putExtra("PRODUCT_ID", id);
        startActivity(intent);
    }

    @Override
    public void onToolBarSetUp(Toolbar toolbar, ActionBar actionBar) {
        actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);
        TextView textView = toolbar.findViewById(R.id.tvHeading);
        textView.setText(R.string.meals_title);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
