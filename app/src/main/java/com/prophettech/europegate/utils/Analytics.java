/*
 * *
 *  * Created by Mohammed Akram Hussain on 03/10/20 13:03
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 03/10/20 12:16
 *
 */

package com.prophettech.europegate.utils;

import android.util.Log;

public class Analytics {


    public static void log(Class callingClass, String message) {
        Log.d(callingClass.getSimpleName(), message);
    }


    public static void log(Class callingClass, Throwable e) {
        Log.e(callingClass.getSimpleName(), e.getMessage(), e);
    }
}
