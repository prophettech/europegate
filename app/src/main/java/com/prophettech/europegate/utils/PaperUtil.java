/*
 * *
 *  * Created by Mohammed Akram Hussain on 03/10/20 13:03
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 03/10/20 12:16
 *
 */

package com.prophettech.europegate.utils;

import android.util.Log;

import com.prophettech.europegate.models.cart.Cart;
import com.prophettech.europegate.models.cart.CartItem;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import io.paperdb.Book;
import io.paperdb.Paper;

public class PaperUtil {

    private static final String TAG = PaperUtil.class.getSimpleName();


    private static final String CART_KEY = "cartItemKey";
    private static final String FIREBASE_KEY = "firebaseKey";


    public static boolean isFirebaseTokenUpdated() {
        Book paper = getPaper();
        try {
            return paper == null ? false : paper.read(FIREBASE_KEY, false);
        } catch (Exception e) {
            Log.d(TAG, e.getMessage());
            return false;
        }
    }

    public static void setFirebaseTokenUpdated(boolean updated) {
        Book paper = getPaper();
        if (paper != null) {
            paper.write(FIREBASE_KEY, updated);
        }
    }

    public static void setCartItem(CartItem cartItem) {
        Book paper = getPaper();
        if (paper != null) {
            List<CartItem> cartItems;
            Cart cart = new Cart();
            try {
                cart = paper.read(CART_KEY, new Cart());
                cartItems = cart.getProducts();
            } catch (Exception e) {
                cartItems = new ArrayList<>();
                Log.d(TAG, e.getMessage());
            }

            if (cartItems == null) {
                cartItems = new ArrayList<>();
            }
            cartItems.add(cartItem);
            cart.setProducts(cartItems);

            paper.write(CART_KEY, cart);
        }
    }

    public static Cart getCartItems() {
        Book paper = getPaper();
        try {
            return paper == null ? null : paper.read(CART_KEY, null);
        } catch (Exception e) {
            Log.d(TAG, e.getMessage());
            return null;
        }
    }


    public static void updateCartItem(CartItem cartItem) {
        Book paper = getPaper();
        if (paper != null) {
            List<CartItem> cartItems;
            Cart cart = new Cart();
            try {
                cart = paper.read(CART_KEY, new Cart());
                cartItems = cart.getProducts();
            } catch (Exception e) {
                cartItems = new ArrayList<>();
                Log.d(TAG, e.getMessage());
            }

            for (CartItem item : cartItems) {
                if (cartItem.getProductId() == item.getProductId()) {
                    item.setQuantity(cartItem.getQuantity());
                    item.setPrice(cartItem.getPrice());
                }
            }
            cart.setProducts(cartItems);
            paper.write(CART_KEY, cart);
        }
    }

    public static void deleteItem(CartItem cartItem) {
        Book paper = getPaper();
        if (paper != null) {
            List<CartItem> cartItems;
            Cart cart = new Cart();
            try {
                cart = paper.read(CART_KEY, new Cart());
                cartItems = cart.getProducts();
            } catch (Exception e) {
                cartItems = new ArrayList<>();
                Log.d(TAG, e.getMessage());
            }
            Iterator<CartItem> iter = cartItems.iterator();
            while (iter.hasNext()) {
                CartItem p = iter.next();
                if (p.getProductId() == cartItem.getProductId()) {
                    iter.remove();
                }
            }
            cart.setProducts(cartItems);
            paper.write(CART_KEY, cart);
        }
    }

    public static void updateCart(Cart updatedCart) {
        Book paper = getPaper();
        if (paper != null) {
            Cart cart = new Cart();
            try {
                cart = paper.read(CART_KEY, new Cart());
            } catch (Exception e) {
                Log.d(TAG, e.getMessage());
            }
            cart.setTotalQuantity(updatedCart.getTotalQuantity());
            cart.setTotalPrice(updatedCart.getTotalPrice());
            cart.setProducts(updatedCart.getProducts());
            paper.write(CART_KEY, cart);
        }
    }


    private static Book getPaper() {
        try {
            return Paper.book();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
